use anyhow::Result;
use serde::Deserialize;
use serde_json::Value;

#[derive(Deserialize)]
pub struct Json;

impl Json {
	pub fn process(&self, input: Vec<u8>, _clean: bool) -> Result<Vec<u8>> {
		let value: Value = serde_json::from_slice(&input)?;
		Ok(serde_json::to_vec_pretty(&value).unwrap())
	}
}
