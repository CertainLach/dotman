use std::os::unix::prelude::OsStrExt;

use crate::{StringMatch, StringMatcher};
use anyhow::Result;
use kconf::{GroupName, KeyLocation};
use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct KdeConfig {
	#[serde(default)]
	remove_keys: Vec<StringMatch>,
	#[serde(default)]
	remove_groups: Vec<StringMatch>,
	#[serde(default)]
	split_keys: Vec<StringMatch>,
	#[serde(default)]
	split_groups: Vec<StringMatch>,
}

impl KdeConfig {
	pub fn process(&self, input: Vec<u8>, clean: bool) -> Result<Vec<u8>> {
		let hostname = hostname::get().unwrap();
		let hostname = hostname.as_bytes();

		let dotman_host_prefix = GroupName::from_segments(&["DotmanHost".as_bytes(), hostname]);

		let split_groups = StringMatcher::new(self.split_groups.clone());
		let remove_groups = StringMatcher::new(self.remove_groups.clone());
		let split_keys = StringMatcher::new(self.split_keys.clone());
		let remove_keys = StringMatcher::new(self.remove_keys.clone());

		let mut kde = kconf::dumb::StandaloneDumbParser::parse(&input)?;

		let mut keys_to_remove: Vec<KeyLocation> = Vec::new();
		let mut groups_to_remove: Vec<GroupName> = Vec::new();
		let mut keys_to_split: Vec<KeyLocation> = Vec::new();
		let mut groups_to_split: Vec<GroupName> = Vec::new();
		let mut groups_to_restore: Vec<GroupName> = Vec::new();
		'group: for (group, v) in kde.groups.iter() {
			if !clean && group.starts_with(&dotman_host_prefix) {
				groups_to_restore.push(group.clone());
				continue 'group;
			}
			if clean && split_groups.matches(&group.to_bytes_with_sep(b'/')) {
				groups_to_split.push(group.clone());
				continue 'group;
			}
			if remove_groups.matches(&group.to_bytes_with_sep(b'/')) {
				groups_to_remove.push(group.clone());
				continue 'group;
			}
			'key: for key in v.entries.keys() {
				let serialized = KeyLocation(group.clone(), key.clone()).to_bytes_with_sep(b'/');
				if clean && split_keys.matches(&serialized) {
					keys_to_split.push(KeyLocation(group.clone(), key.clone()));
					continue 'key;
				}
				if remove_keys.matches(&serialized) {
					keys_to_remove.push(KeyLocation(group.clone(), key.clone()));
					continue 'key;
				}
			}
		}
		for key in keys_to_remove {
			kde.remove_key(&key).unwrap();
		}
		for group in groups_to_remove {
			kde.remove_group(&group).unwrap();
		}
		if clean {
			for mut key in keys_to_split {
				let value = kde.remove_key(&key).unwrap();
				let mut group = dotman_host_prefix.clone();
				group.append(&key.0);
				key.0 = group;
				kde.insert_key(key, value);
			}
			for group in groups_to_split {
				let value = kde.remove_group(&group).unwrap();
				let mut group_prefixed = dotman_host_prefix.clone();
				group_prefixed.append(&group);
				kde.extend_with_group(group_prefixed, value);
			}
		}
		if !clean {
			for group in groups_to_restore {
				let value = kde.remove_group(&group).unwrap();
				kde.extend_with_group(
					GroupName::from_segments(
						&group
							.segments()
							.skip(dotman_host_prefix.segments().count())
							.collect::<Vec<_>>(),
					),
					value,
				);
			}
		}
		Ok(kde.serialize()?)
	}
}
