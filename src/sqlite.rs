use anyhow::Result;
use rusqlite::{params, types::Value, Connection};
use serde::Deserialize;
use std::io::Write;

fn write_table_rows(conn: &Connection, table: &str, out: &mut Vec<u8>) -> Result<()> {
	let mut table_stmt = conn.prepare(&format!("SELECT * FROM {}", table))?;
	let column_count = table_stmt.column_count();
	let column_names = table_stmt
		.column_names()
		.into_iter()
		.map(|c| c.to_owned())
		.collect::<Vec<_>>();
	let mut table_rows = table_stmt.query(params![])?;
	let mut first = Some(());
	while let Some(table_row) = table_rows.next()? {
		if first.take().is_some() {
			write!(out, "INSERT INTO {} (", table)?;
			for (i, column) in column_names.iter().enumerate() {
				if i != 0 {
					write!(out, ", ")?;
				}
				write!(out, "{}", column)?;
			}
			writeln!(out, ") VALUES")?;
		} else {
			writeln!(out, ",")?;
		}
		write!(out, "\t(")?;
		for column in 0..column_count {
			if column != 0 {
				write!(out, ", ")?;
			}
			let value: Value = table_row.get(column)?;
			match value {
				Value::Null => write!(out, "NULL")?,
				Value::Integer(i) => write!(out, "{}", i)?,
				Value::Real(_) => todo!(),
				Value::Text(t) => write!(out, "{:?}", t)?,
				Value::Blob(b) => write!(out, "x'{}'", hex::encode(&b))?,
			}
		}
		write!(out, ")")?;
	}
	if first.take().is_none() {
		writeln!(out, ";")?;
	}
	Ok(())
}

#[derive(Deserialize)]
pub struct Sqlite {}
impl Sqlite {
	pub fn process(&self, input: Vec<u8>, clean: bool) -> Result<Vec<u8>> {
		let mut temp = tempfile::NamedTempFile::new()?;

		if clean {
			let mut out = Vec::new();
			temp.write_all(&input)?;
			let conn = Connection::open(&temp)?;

			let mut statement = conn.prepare(
				r#"
					SELECT 
						type, name, tbl_name, sql, 
						CASE
							WHEN type=='trigger' THEN 3
							WHEN type=='index' THEN 2
							WHEN type=='table' THEN 1
							ELSE 0
						END AS ordering
					FROM sqlite_master 
					ORDER BY tbl_name, ordering, name
				"#,
			)?;

			let mut rows = statement.query(params![])?;
			let mut last_table = None;
			while let Some(row) = rows.next()? {
				let table = row.get::<_, String>("tbl_name")?;
				if last_table.as_ref() != Some(&table) {
					if let Some(last_table) = last_table {
						write_table_rows(&conn, &last_table, &mut out)?;
					}
				}
				last_table = Some(table);
				if let Some(sql) = row.get::<_, Option<String>>("sql")? {
					writeln!(out, "{};", sql.trim())?;
				}
			}
			if let Some(last_table) = last_table {
				write_table_rows(&conn, &last_table, &mut out)?;
			}

			Ok(out)
		} else {
			{
				let mut conn = Connection::open(&temp)?;
				conn.pragma_update(None, "foreign_keys", &false)?;
				let tx = conn.transaction()?;
				let query = String::from_utf8(input)?;
				tx.execute_batch(&query)?;
				tx.commit()?;
			}
			Ok(std::fs::read(&temp)?)
		}
	}
}
