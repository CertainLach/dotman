use anyhow::{bail, Context, Result};
use git_filter_server::{GitFilterServer, ProcessingType, Processor};
use jrsonnet_evaluator::FileImportResolver;
use regex::bytes::{Regex, RegexSet};
use serde::Deserialize;
use serde_json::Value;
use std::{
	collections::{BTreeMap, HashSet},
	convert::TryInto,
	fs::OpenOptions,
	io::{self, BufRead, BufReader},
	io::{Read, Write},
	path::PathBuf,
	process::Command,
};
use structopt::clap::AppSettings::ColoredHelp;
use structopt::StructOpt;

mod json;
mod kde;
mod sqlite;

#[derive(StructOpt, Debug)]
#[structopt(name = "dotman", author, global_setting(ColoredHelp))]
enum Opts {
	/// Internal command, which starts git filter server
	Git,
	/// Configure git to use
	Init,
}

#[derive(Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
enum StringMatch {
	Exact(String),
	#[serde(with = "serde_regex")]
	Regex(Regex),
}

struct StringMatcher(RegexSet);
impl StringMatcher {
	fn new(data: Vec<StringMatch>) -> Self {
		let mut out = vec![];
		for r in data {
			match r {
				StringMatch::Exact(s) => out.push(regex::escape(&s)),
				StringMatch::Regex(r) => out.push(r.as_str().to_owned()),
			}
		}
		Self(RegexSet::new(out).unwrap())
	}

	fn matches(&self, input: &[u8]) -> bool {
		self.0.is_match(input)
	}
}

#[derive(Deserialize)]
#[serde(tag = "type", rename_all = "camelCase")]
enum Processing {
	KdeConfig(kde::KdeConfig),
	Json(json::Json),
	Sqlite(sqlite::Sqlite),
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct DotmanConfig {
	files: BTreeMap<String, Processing>,
}

struct ConfigProcessor(DotmanConfig);
impl Processor for ConfigProcessor {
	fn process<R: Read, W: Write>(
		&mut self,
		pathname: &str,
		process_type: ProcessingType,
		input: &mut R,
		output: &mut W,
	) -> Result<()> {
		if let Some(proc) = self.0.files.get(pathname) {
			let clean = matches!(process_type, ProcessingType::Clean);
			let mut data = Vec::new();
			input.read_to_end(&mut data)?;
			let data = match proc {
				Processing::KdeConfig(k) => k.process(data, clean)?,
				Processing::Json(j) => j.process(data, clean)?,
				Processing::Sqlite(s) => s.process(data, clean)?,
			};
			output.write_all(&data)?;
		} else {
			let mut data = Vec::new();
			input.read_to_end(&mut data)?;
			output.write_all(&data)?;
		}
		Ok(())
	}
	fn supports_processing(&self, process_type: ProcessingType) -> bool {
		matches!(process_type, ProcessingType::Clean | ProcessingType::Smudge)
	}
}

fn main() -> Result<()> {
	tracing_subscriber::fmt::fmt()
		.with_writer(io::stderr)
		.init();

	let opts = Opts::from_args();

	match opts {
		Opts::Init => {
			if !std::fs::metadata(".git")
				.map(|f| f.is_dir())
				.unwrap_or(false)
			{
				bail!("dotman init should be called in root of git repo");
			}
			Command::new("git")
				.args(["config", "--bool", "filter.dotman.required", "true"])
				.output()?;
			Command::new("git")
				.args(["config", "filter.dotman.process", "dotman git"])
				.output()?;
			let attributes = OpenOptions::new()
				.read(true)
				.write(true)
				.create(true)
				.open(".gitattributes")
				.context("while creating .gitattributes")?;
			let attributes_reader = BufReader::new(attributes);
			let lines: HashSet<String> = attributes_reader.lines().flatten().collect();

			let mut attributes = OpenOptions::new().append(true).open(".gitattributes")?;

			if !lines.contains("* filter=dotman") {
				writeln!(attributes, "* filter=dotmant")?;
			}
		}
		Opts::Git => {
			let state = jrsonnet_evaluator::EvaluationState::default();
			let value = match state.run_in_state(|| {
				state.set_import_resolver(Box::new(FileImportResolver::default()));
				state.with_stdlib();
				let file = state.evaluate_file_raw(&PathBuf::from("dotman.jsonnet"))?;
				let value: Value = (&file).try_into()?;
				Ok(value) as jrsonnet_evaluator::error::Result<Value>
			}) {
				Ok(v) => v,
				Err(e) => {
					let s = state.stringify_err(&e);
					bail!("failed to load dotman.jsonnet config:\n{}", s);
				}
			};

			let config: DotmanConfig = serde_json::from_value(value)?;

			GitFilterServer::new(ConfigProcessor(config)).communicate_stdio()?;
		}
	};
	Ok(())
}
