use enumflags2::BitFlags;
use log::warn;
use std::collections::BTreeMap;
use std::io::Write;

use crate::KeyLocation;
use crate::{parse_config, EntryFlags, GroupName, KeyName, ParseCallback, ParseWarning, Value};

#[derive(Debug)]
pub struct StandaloneDumbEntryData {
	flags: BitFlags<EntryFlags>,
	value: Value,
}
impl StandaloneDumbEntryData {
	fn serialize_buf(
		&self,
		locale: Option<&KeyName>,
		printable_name: &[u8],
		out: &mut Vec<u8>,
	) -> std::io::Result<()> {
		out.write_all(printable_name)?;
		if let Some(locale) = locale {
			write!(out, "[")?;
			locale.serialize_buf(out)?;
			write!(out, "]")?;
		}
		serialize_flags(&self.flags, out)?;
		if !self.flags.contains(EntryFlags::Deleted) {
			write!(out, "=")?;
			self.value.serialize_buf(out)?;
		}
		Ok(())
	}
}

#[derive(Debug, Default)]
pub struct StandaloneDumbEntry(BTreeMap<Option<KeyName>, StandaloneDumbEntryData>);
impl StandaloneDumbEntry {
	fn serialize_buf(&self, name: &KeyName, out: &mut Vec<u8>) -> std::io::Result<()> {
		let mut printable_name = Vec::new();
		name.serialize_buf(&mut printable_name)?;

		for (k, v) in self.0.iter() {
			v.serialize_buf(k.as_ref(), &printable_name, out)?;
			writeln!(out)?;
		}
		Ok(())
	}
}

fn serialize_flags(flags: &BitFlags<EntryFlags>, out: &mut Vec<u8>) -> std::io::Result<()> {
	if flags.iter().any(|flag| flag.visible()) {
		write!(out, "[$")?;
		if flags.contains(EntryFlags::Deleted) {
			write!(out, "d")?;
		}
		if flags.contains(EntryFlags::Immutable) {
			write!(out, "i")?;
		}
		if flags.contains(EntryFlags::Expansion) {
			write!(out, "e")?;
		}
		write!(out, "]")?;
	}
	Ok(())
}

#[derive(Debug, Default)]
pub struct StandaloneDumbGroup {
	immutable: bool,
	pub entries: BTreeMap<KeyName, StandaloneDumbEntry>,
}
impl StandaloneDumbGroup {
	fn serialize_buf(&self, out: &mut Vec<u8>) -> std::io::Result<()> {
		for (k, v) in &self.entries {
			v.serialize_buf(k, out)?;
		}
		Ok(())
	}
}

#[derive(Debug)]
pub struct StandaloneDumbParser {
	immutable_file: bool,
	current_line: usize,
	pub groups: BTreeMap<GroupName, StandaloneDumbGroup>,
	current_group: GroupName,
}
impl StandaloneDumbParser {
	pub fn parse(input: &[u8]) -> std::io::Result<Self> {
		let mut out = StandaloneDumbParser {
			immutable_file: false,
			current_line: 0,
			groups: BTreeMap::new(),
			current_group: GroupName(Vec::new()),
		};
		parse_config(input, &mut out)?;
		Ok(out)
	}
	pub fn serialize_buf(&self, out: &mut Vec<u8>) -> std::io::Result<()> {
		let default_name = GroupName(b"<default>".to_vec());
		let mut first = true;
		if let Some(default) = self.groups.get(&default_name) {
			if !default.entries.is_empty() {
				first = false;
				default.serialize_buf(out)?;
			}
		}
		if self.immutable_file {
			if !first {
				writeln!(out)?;
			}
			first = false;
			writeln!(out, "[$i]")?
		}
		let empty_name = GroupName(b"".to_vec());
		if let Some(default) = self.groups.get(&empty_name) {
			if !default.entries.is_empty() {
				first = false;
				default.serialize_buf(out)?;
			}
		}
		for (k, v) in self.groups.iter() {
			if k == &default_name || k == &empty_name {
				continue;
			}
			if !v.immutable && v.entries.is_empty() {
				continue;
			}
			if !first {
				writeln!(out)?;
			}
			first = false;
			k.serialize_buf(out)?;
			if v.immutable {
				write!(out, "[$i]")?;
			}
			writeln!(out)?;
			v.serialize_buf(out)?;
		}
		Ok(())
	}
	pub fn serialize(&self) -> std::io::Result<Vec<u8>> {
		let mut out = Vec::new();
		self.serialize_buf(&mut out)?;
		Ok(out)
	}
	pub fn remove_key(&mut self, loc: &KeyLocation) -> Option<StandaloneDumbEntry> {
		self.groups
			.get_mut(&loc.0)
			.map(|group| group.entries.remove(&loc.1))
			.flatten()
	}
	pub fn insert_key(
		&mut self,
		loc: KeyLocation,
		value: StandaloneDumbEntry,
	) -> Option<StandaloneDumbEntry> {
		self.groups
			.entry(loc.0)
			.or_default()
			.entries
			.insert(loc.1, value)
	}

	pub fn remove_group(&mut self, loc: &GroupName) -> Option<StandaloneDumbGroup> {
		self.groups.remove(loc)
	}

	pub fn extend_with_group(&mut self, loc: GroupName, with_group: StandaloneDumbGroup) {
		let group = self.groups.entry(loc).or_default();
		group.entries.extend(with_group.entries.into_iter());
	}
}
impl ParseCallback for StandaloneDumbParser {
	fn line_started(&mut self, line: usize) {
		self.current_line = line;
	}

	fn warn(&mut self, pos: usize, warn: ParseWarning) {
		warn!("{}:{}: {}", self.current_line, pos, warn);
	}

	fn group_started(&mut self, group: GroupName, immutable: bool) {
		self.current_group = group.clone();
		let value = self.groups.entry(group).or_default();
		if immutable {
			value.immutable = true;
		}
	}

	fn mark_file_as_immutable(&mut self) {
		self.immutable_file = true;
	}

	fn set_entry(
		&mut self,
		name: KeyName,
		locale: Option<KeyName>,
		value: Value,
		flags: BitFlags<EntryFlags>,
	) {
		let group = self.groups.get_mut(&self.current_group).unwrap();
		group
			.entries
			.entry(name)
			.or_default()
			.0
			.insert(locale, StandaloneDumbEntryData { flags, value });
	}
}
