//! String escaping functions

use std::cell::Cell;

use crate::ParseWarning;

#[derive(Debug)]
pub enum StringType {
	Group,
	Key,
	Value,
}

impl StringType {
	// Should preserve leading spaces
	fn protect_trailing_spaces(&self) -> bool {
		matches!(self, Self::Group)
	}
	// Should = be escaped
	fn escape_equals(&self) -> bool {
		matches!(self, Self::Key)
	}
	// Should [, ] be escaped
	fn escape_group_chars(&self) -> bool {
		matches!(self, Self::Key | Self::Group)
	}
}

fn escape_buf(input: u8, out: &mut Vec<u8>) -> std::io::Result<()> {
	use std::io::Write;

	write!(out, "\\x{:02x?}", input)
}

/// Should behave same way as https://invent.kde.org/frameworks/kconfig/-/blob/dcc5f6529d3008f2cf3a678a4c42d5092b406690/src/core/kconfigini.cpp#L747
pub fn string_to_printable_buf(
	input: &[u8],
	out: &mut Vec<u8>,
	str_type: StringType,
) -> std::io::Result<()> {
	use std::io::Write;

	let mut input = input.iter().cloned().peekable();

	if str_type.protect_trailing_spaces() {
		while input.peek() == Some(&b' ') {
			input.next();
			out.extend_from_slice(b"\\s");
		}
	}

	while let Some(v) = input.next() {
		match v {
			b'\n' => write!(out, "\\n")?,
			b'\t' => write!(out, "\\t")?,
			b'\r' => write!(out, "\\r")?,
			b'\\' => write!(out, "\\\\")?,
			b'=' if str_type.escape_equals() => escape_buf(b'=', out)?,
			v @ (b'[' | b']') if str_type.escape_group_chars() => escape_buf(v, out)?,
			v => {
				let mut out_utf = Vec::new();
				out_utf.push(v);
				loop {
					match std::str::from_utf8(&out_utf) {
						Ok(_) => {
							out.extend(&out_utf);
							break;
						}
						// Incomplete utf8
						Err(e) if e.error_len().is_none() && input.peek().is_some() => {
							out_utf.push(input.next().unwrap());
							continue;
						}
						// Invalid utf8
						Err(_) => {
							for b in out_utf {
								escape_buf(b, out)?;
							}
							break;
						}
					}
				}
			}
		}
	}

	// Replace spaces at the end of string with \s
	if str_type.protect_trailing_spaces() {
		let spaces = out.iter_mut().rev().take_while(|v| v == &&b' ').count();
		for (i, pos) in ((out.len() - spaces)..out.len()).into_iter().enumerate() {
			out[pos] = if i % 2 == 0 { b'\\' } else { b's' }
		}
		for (i, _) in (0..spaces).into_iter().enumerate() {
			out.push(if i % 2 == 1 { b'\\' } else { b's' })
		}
	}

	Ok(())
}

pub fn string_to_printable(input: &[u8], str_type: StringType) -> std::io::Result<Vec<u8>> {
	let mut out = Vec::new();
	string_to_printable_buf(input, &mut out, str_type)?;
	Ok(out)
}

fn read_hex_char(c: u8) -> Option<u8> {
	Some(match c {
		c @ b'0'..=b'9' => c - b'0',
		c @ b'a'..=b'f' => c - b'a' + 10,
		c @ b'A'..=b'F' => c - b'A' + 10,
		_ => return None,
	})
}
fn read_hex_byte(input: &mut impl Iterator<Item = u8>) -> Result<u8, ParseWarning> {
	match (input.next(), input.next()) {
		(Some(a), Some(b)) => {
			let a = read_hex_char(a).ok_or(ParseWarning::BadHexChar(a))?;
			let b = read_hex_char(b).ok_or(ParseWarning::BadHexChar(b))?;
			Ok((a << 4) | b)
		}
		// Not really sure why it is handled this way https://invent.kde.org/frameworks/kconfig/-/blob/dcc5f6529d3008f2cf3a678a4c42d5092b406690/src/core/kconfigini.cpp#L910
		(v, _) => Err(ParseWarning::UnexpectedEndOfEscape(v)),
	}
}

pub fn printable_to_string_buf(
	input: &[u8],
	out: &mut Vec<u8>,
	mut warn: impl FnMut(usize, ParseWarning),
) -> std::io::Result<()> {
	let current_pos = Cell::new(0);

	let mut input = input
		.iter()
		.cloned()
		.peekable()
		.enumerate()
		.map(|(pos, v)| {
			current_pos.set(pos);
			v
		});

	while let Some(v) = input.next() {
		if v != b'\\' {
			out.push(v);
		} else {
			match input.next() {
				// Input may end right after \\
				None => {
					warn(current_pos.get(), ParseWarning::MissingEscapedChar);
					out.push(b'\\');
				}
				Some(b'\\') => out.push(b'\\'),
				Some(b's') => out.push(b' '),
				Some(b't') => out.push(b'\t'),
				Some(b'r') => out.push(b'\r'),
				// not really an escape sequence, but allowed in .desktop files
				Some(c @ (b';' | b',')) => {
					out.push(b'\\');
					out.push(c);
				}
				Some(b'x') => {
					let byte = read_hex_byte(&mut input).unwrap_or_else(|e| {
						warn(current_pos.get(), e);
						b'x'
					});
					out.push(byte)
				}
				Some(other) => {
					warn(
						current_pos.get(),
						ParseWarning::InvalidEscapeSequence(other),
					);
					out.push(b'\\');
				}
			}
		}
	}

	Ok(())
}

pub type Warnings = Vec<(usize, ParseWarning)>;

pub fn printable_to_string(input: &[u8]) -> std::io::Result<(Vec<u8>, Warnings)> {
	let mut out = Vec::new();
	let mut warnings = Vec::new();
	printable_to_string_buf(input, &mut out, |pos, warn| warnings.push((pos, warn)))?;
	Ok((out, warnings))
}

#[cfg(test)]
pub mod tests {
	use super::*;
	use rstest::*;

	#[rstest]
	#[case::keys_have_escaped_group_chars("Aaa[Bbb]", r"Aaa\x5bBbb\x5d", StringType::Key)]
	#[case::keys_have_escaped_equals("Aaa=Bbb", r"Aaa\x3dBbb", StringType::Key)]
	#[case::groups_preserves_trailing_spaces("   AAA   ", r"\s\s\sAAA\s\s\s", StringType::Group)]
	#[trace]
	fn test_escape(
		#[case] input: &str,
		#[case] output: &str,
		#[case] str_type: StringType,
	) -> anyhow::Result<()> {
		let printable = string_to_printable(input.as_bytes(), str_type)?;
		let printable = String::from_utf8(printable)?;

		assert_eq!(printable, output);
		Ok(())
	}

	#[rstest]
	#[case::bad_utf8_is_escaped(b"\xff", b"\\xff", StringType::Key)]
	// \xc3\x28 is invalid utf-8 two-byte escape
	#[case::bad_utf8_is_escaped(b"\xc3\x28", b"\\xc3\\x28", StringType::Key)]
	// b'a' == 0x61
	// b'(' == 0x28
	#[case::bad_utf8_is_escaped(b"\xc3a\x28", b"\\xc3\\x61(", StringType::Key)]
	#[trace]
	fn test_escape_raw(
		#[case] input: &[u8],
		#[case] output: &[u8],
		#[case] str_type: StringType,
	) -> anyhow::Result<()> {
		let printable = string_to_printable(input, str_type)?;
		println!("Escaped {:?}", std::str::from_utf8(&printable));
		assert_eq!(printable, output);
		Ok(())
	}

	#[rstest]
	#[case::bad_escapes_are_replaced("\\xf", "x")]
	#[case::bad_escapes_are_replaced("\\xkf", "x")]
	#[case::missing_escapes_are_replaced("\\x", "x")]
	#[trace]
	fn test_parse(#[case] input: &str, #[case] output: &str) -> anyhow::Result<()> {
		let (string, warnings) = printable_to_string(input.as_bytes())?;

		for warning in warnings {
			println!("{}: {}", warning.0, warning.1)
		}

		let string = String::from_utf8(string)?;

		assert_eq!(string, output);
		Ok(())
	}
}
