use std::fmt::Debug;

use string::{string_to_printable_buf, StringType};
use thiserror::Error;

use crate::string::printable_to_string_buf;
use enumflags2::{bitflags, BitFlags};

pub mod dumb;
pub mod string;

const GROUP_SEPARATOR: u8 = 0x1d;

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone)]
pub struct GroupName(Vec<u8>);
impl GroupName {
	pub fn new() -> Self {
		Self(Vec::new())
	}
	pub fn from_segments<T: AsRef<[u8]>>(segments: &[T]) -> Self {
		let mut out = Self::new();
		for seg in segments {
			out.push(seg);
		}
		out
	}
	fn serialize_buf(&self, out: &mut Vec<u8>) -> std::io::Result<()> {
		for group in self.0.split(|b| *b == GROUP_SEPARATOR) {
			out.push(b'[');
			string_to_printable_buf(group, out, StringType::Group)?;
			out.push(b']');
		}
		Ok(())
	}
	pub fn pop(&mut self) {
		self.0.truncate(
			self.0
				.iter()
				.rposition(|c| *c == GROUP_SEPARATOR)
				.unwrap_or(0),
		)
	}
	pub fn push(&mut self, segment: impl AsRef<[u8]>) {
		if !self.0.is_empty() {
			self.0.push(GROUP_SEPARATOR);
		}
		self.0.extend_from_slice(segment.as_ref());
	}
	pub fn append(&mut self, group: &GroupName) {
		self.push(&group.0)
	}
	pub fn then(&self, segment: impl AsRef<[u8]>) -> Self {
		let mut out = self.clone();
		out.push(segment);
		out
	}
	pub fn segments(&self) -> impl Iterator<Item = &[u8]> {
		self.0.split(|c| *c == GROUP_SEPARATOR)
	}
	pub fn ends_with(&self, group: &Self) -> bool {
		self.0.ends_with(&group.0)
	}
	pub fn starts_with(&self, group: &Self) -> bool {
		self.0.starts_with(&group.0)
	}
	pub fn as_bytes(&self) -> &[u8] {
		&self.0
	}
	pub fn to_bytes_with_sep(&self, sep: u8) -> Vec<u8> {
		let mut first = true;
		let mut out = Vec::new();
		for seg in self.segments() {
			if !first {
				out.push(sep);
			}
			first = false;
			out.extend_from_slice(seg);
		}
		out
	}
}

impl Debug for GroupName {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for group in self.0.split(|b| *b == GROUP_SEPARATOR) {
			write!(f, ".")?;
			write!(f, "{}", String::from_utf8_lossy(group))?;
		}
		Ok(())
	}
}

impl Default for GroupName {
	fn default() -> Self {
		GroupName(b"<default>".to_vec())
	}
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone)]
pub struct KeyName(pub Vec<u8>);
impl KeyName {
	fn serialize_buf(&self, out: &mut Vec<u8>) -> std::io::Result<()> {
		string_to_printable_buf(&self.0, out, StringType::Key)?;
		Ok(())
	}
}

impl Debug for KeyName {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "\"")?;
		write!(f, "{}", String::from_utf8_lossy(&self.0))?;
		write!(f, "\"")
	}
}

#[derive(Debug)]
pub struct KeyLocation(pub GroupName, pub KeyName);
impl KeyLocation {
	pub fn to_bytes_with_sep(&self, sep: u8) -> Vec<u8> {
		let mut first = true;
		let mut out = Vec::new();
		for seg in self.0.segments() {
			if !first {
				out.push(sep);
			}
			first = false;
			out.extend_from_slice(seg);
		}
		if !first {
			out.push(sep);
		}
		out.extend_from_slice(&self.1 .0);
		out
	}
	pub fn from_bytes_with_sep(&self, bytes: &[u8], sep: u8) -> Self {
		if let Some(key_start) = bytes.iter().rposition(|c| *c == sep) {
			Self(
				GroupName(bytes[..key_start].to_vec()),
				KeyName(bytes[key_start + 1..].to_vec()),
			)
		} else {
			Self(GroupName::default(), KeyName(bytes.to_vec()))
		}
	}
}

pub struct Value(Vec<u8>);
impl Value {
	fn serialize_buf(&self, out: &mut Vec<u8>) -> std::io::Result<()> {
		string_to_printable_buf(&self.0, out, StringType::Value)?;
		Ok(())
	}
}
impl Debug for Value {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "\"")?;
		write!(f, "{}", String::from_utf8_lossy(&self.0))?;
		write!(f, "\"")
	}
}

fn trim_bytes(input: &[u8]) -> &[u8] {
	let start_idx = input
		.iter()
		.position(|c| !(*c as char).is_whitespace())
		.unwrap_or(input.len());
	let end_idx = input
		.iter()
		.rposition(|c| !(*c as char).is_whitespace())
		.map(|c| c + 1)
		.unwrap_or(input.len());
	&input[start_idx..end_idx]
}

#[derive(Error, Debug)]
pub enum ParseWarning {
	#[error("unexpected end of escape, got \\x{}_", .0.map(|c| format!("{}", c as char)).unwrap_or_else(||"_".to_owned()))]
	UnexpectedEndOfEscape(Option<u8>),
	#[error("bad hex char: {}", *.0 as char)]
	BadHexChar(u8),
	#[error("invalid escape sequence")]
	InvalidEscapeSequence(u8),
	#[error("missing escape character")]
	MissingEscapedChar,
	#[error("missing group end")]
	MissingGroupEnd,
	#[error("empty key")]
	EmptyKey,
	#[error("missing ']'")]
	MissingTagEnd,
	#[error("missing '='")]
	MissingEquals,
	/// Because default group has broken parsing with immutable files
	#[error("key is located in default group")]
	KeyIsLocatedInDefaultGroup,

	#[error("skipped data: {0:?}")]
	SkippedData(Vec<u8>),
	#[error("locale is specified multiple times?")]
	SecondLocale,
}

pub trait ParseCallback {
	fn line_started(&mut self, line: usize);
	/// Called on parse warning
	/// There shouldn't be faulty files, only with warnings
	fn warn(&mut self, pos: usize, warn: ParseWarning);
	/// Called when group is started
	/// Group can be restarted as many times he wants
	fn group_started(&mut self, group: GroupName, immutable: bool);
	/// Called on `[$i]` line, which can be located anywhere
	fn mark_file_as_immutable(&mut self);

	fn set_entry(
		&mut self,
		name: KeyName,
		locale: Option<KeyName>,
		value: Value,
		flags: BitFlags<EntryFlags>,
	);
}

pub fn parse_group(mut line: &[u8], cb: &mut impl ParseCallback) -> std::io::Result<bool> {
	let mut group_name = Vec::new();
	while line.starts_with(b"[") {
		line = &line[1..];
		if let Some(pos) = line.iter().position(|b| *b == b']') {
			let group = &line[..pos];
			line = &line[pos + 1..];
			if !group_name.is_empty() {
				group_name.push(0x1d);
			}
			printable_to_string_buf(group, &mut group_name, |pos, warn| cb.warn(pos, warn))?;
		} else {
			cb.warn(0, ParseWarning::MissingGroupEnd)
		}
	}

	let mut group_is_switched_default = false;
	let mut group_is_immutable = false;
	// File immutable flag doesn't skips current group creation, so
	// ```
	// AAA=3
	// [$i]
	// BBB=4
	// ```
	// Is equal to
	// [$i]
	// [<default>]
	// AAA=3
	// []
	// BBB=4
	if group_name == b"$i" {
		group_name.clear();
		cb.mark_file_as_immutable();
	} else if group_name.ends_with(b"\x1d$i") {
		group_is_immutable = true;
		for _ in 0..3 {
			group_name.pop();
		}
		group_is_switched_default = true;
	} else {
		group_is_switched_default = true;
	}

	cb.group_started(GroupName(group_name), group_is_immutable);
	Ok(group_is_switched_default)
}

#[bitflags]
#[repr(u16)]
#[derive(Clone, Copy, Debug)]
pub enum EntryFlags {
	Dirty,
	Global,
	Immutable,
	Deleted,
	Expansion,
	Default,
	Localized,
	LocalizedCountry,
}
impl EntryFlags {
	pub fn visible(&self) -> bool {
		matches!(self, Self::Immutable | Self::Deleted | Self::Expansion)
	}
}

pub fn parse_key(mut line: &[u8], cb: &mut impl ParseCallback) -> std::io::Result<()> {
	let (no_equals, mut key) = if let Some(end) = line.iter().position(|c| *c == b'=') {
		let key = trim_bytes(&line[..end]);
		line = trim_bytes(&line[end + 1..]);
		(false, key)
	} else {
		let key = line;
		line = &line[0..0];
		// No trim is performed in case of no value
		(true, key)
	};
	if key.is_empty() {
		cb.warn(0, ParseWarning::EmptyKey);
		return Ok(());
	}
	let mut flags = BitFlags::<EntryFlags>::default();
	let mut locale = None;

	while let Some(tag_start) = key.iter().rposition(|c| *c == b'[') {
		let tag_end = match key
			.iter()
			.skip(tag_start)
			.position(|c| *c == b']')
			.map(|p| p + tag_start)
		{
			Some(p) => p,
			None => {
				cb.warn(tag_start, ParseWarning::MissingTagEnd);
				return Ok(());
			}
		};
		if tag_end != key.len() - 1 {
			cb.warn(tag_end, ParseWarning::SkippedData(key[tag_end..].to_vec()));
		}
		let tag = &key[tag_start + 1..tag_end];

		if tag.starts_with(b"$") {
			for (pos, tag_value) in tag[1..].iter().enumerate() {
				match *tag_value {
					b'i' => flags |= EntryFlags::Immutable,
					b'e' => flags |= EntryFlags::Expansion,
					b'd' => {
						flags |= EntryFlags::Deleted;
						key = &key[..tag_start];
						if pos != tag.len() - 1 {
							cb.warn(pos, ParseWarning::SkippedData(tag[pos..].to_vec()))
						}
						let mut key_out = Vec::new();
						printable_to_string_buf(key, &mut key_out, |off, warn| {
							cb.warn(off, warn);
						})?;
						// Locale is ignored here
						cb.set_entry(KeyName(key_out), None, Value(Vec::new()), flags);
						return Ok(());
					}
					_ => {
						cb.warn(pos, ParseWarning::SkippedData(tag[pos..].to_vec()));
						break;
					}
				}
			}
		} else {
			if locale.is_some() {
				cb.warn(tag_start, ParseWarning::SecondLocale);
				return Ok(());
			}
			flags |= EntryFlags::Localized;
			if !tag.iter().any(|t| *t == b'_') {
				flags |= EntryFlags::LocalizedCountry;
			}
			locale = Some(KeyName(tag.to_vec()))
		}

		key = &key[..tag_start];
	}
	if no_equals {
		cb.warn(0, ParseWarning::MissingEquals);
	}
	let mut key_out = Vec::new();
	printable_to_string_buf(key, &mut key_out, |off, warn| {
		cb.warn(off, warn);
	})?;
	let key = KeyName(key_out);

	let mut value_out = Vec::new();
	printable_to_string_buf(line, &mut value_out, |off, warn| {
		cb.warn(off, warn);
	})?;
	let value = Value(value_out);

	cb.set_entry(key, locale, value, flags);

	Ok(())
}

pub fn parse_config(input: &[u8], cb: &mut impl ParseCallback) -> std::io::Result<()> {
	let mut default_group = true;
	cb.group_started(GroupName(b"<default>".to_vec()), false);

	for (line_num, line) in input.split(|b| *b == b'\n').enumerate() {
		cb.line_started(line_num);
		let line = trim_bytes(line);

		if line.is_empty() || line.starts_with(b"#") {
			continue;
		}

		if line.starts_with(b"[") {
			if parse_group(line, cb)? {
				default_group = false;
			}
		} else {
			if default_group {
				cb.warn(0, ParseWarning::KeyIsLocatedInDefaultGroup)
			}
			parse_key(line, cb)?;
		}
	}
	Ok(())
}

#[cfg(test)]
pub mod tests {
	use super::*;

	#[test]
	fn test_trim() {
		assert_eq!(trim_bytes(b"  aaa  "), b"aaa");
		assert_eq!(trim_bytes(b"aaa  "), b"aaa");
		assert_eq!(trim_bytes(b"  aaa"), b"aaa");
		assert_eq!(trim_bytes(b"    "), b"");
	}
}
