{
  description = "Dotfiles manager";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };
  outputs = { self, nixpkgs, flake-utils, rust-overlay, naersk }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs
          {
            inherit system;
            overlays = [ rust-overlay.overlay ];
          };
        rust = ((pkgs.rustChannelOf { date = "2021-08-16"; channel = "nightly"; }).default.override {
          extensions = [ "rust-src" ];
        });
        naersk-lib = naersk.lib."${system}".override {
          rustc = rust;
          cargo = rust;
        };
      in
      {
        defaultPackage = naersk-lib.buildPackage {
          pname = "dotman";
          root = ./.;
          buildInputs = with pkgs; [
            pkgs.sqlite
          ];
        };
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs;[
            pkgs.binutils
            pkgs.pkgconfig
            pkgs.clang
            pkgs.x11
            pkgs.alsaLib
            pkgs.libudev
            pkgs.sqlite
            rust
          ];
        };
      }
    );
}
